# Cách chạy:

1. [Link source trên Gitlab]("https://gitlab.com/itd-groups/Demo-Handsontable");
2. Mở terminal hoặc Gitbash trên Desktop

   - gõ lệnh: git clone https://gitlab.com/itd-groups/Demo-

   - npm install

   - ng serve -o (Tự động mở Angular App)

# Handsontable

[Link tham khảo]("https://handsontable.com/docs/javascript-data-grid/")

1. Setup theo JS

[Link tham khảo]("https://handsontable.com/docs/javascript-data-grid/installation/")

2. Setup theo Angular

[Link tham khảo]("https://handsontable.com/docs/javascript-data-grid/angular-installation/")

**Setup theo ReactJS hoặc VueJS tại**

[ReactJS]("https://handsontable.com/docs/react-data-grid/")
[VueJS]("https://handsontable.com/docs/javascript-data-grid/vue-installation/")
