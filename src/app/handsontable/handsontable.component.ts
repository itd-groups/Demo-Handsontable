import { ApiService } from './../Services/api.service';
import { Component, OnInit } from '@angular/core';
import { HotTableRegisterer } from '@handsontable/angular';
import Handsontable from 'handsontable/base';

@Component({
  selector: 'app-handsontable',
  templateUrl: './handsontable.component.html',
  styleUrls: ['./handsontable.component.scss'],
})
export class HandsontableComponent implements OnInit {
  staffList: any[] = [];
  checked: boolean = false;
  notification: string = 'Đang đợi dữ liệu';
  dataAfterChanges: any[] = [];
  columnWidths: any[] = [];

  private hotRegisterer = new HotTableRegisterer();

  constructor(private apiService: ApiService) {}

  public settings: Handsontable.GridSettings = {
    columns: [
      {
        data: 0,
      },
      {
        data: 1,
        type: 'date',
        allowInvalid: false,
        dateFormat: 'DD/MM/YYYY',
      },
      { data: 2 },
      { data: 3 },
    ],
    startCols: 5,
    startRows: 6,
    licenseKey: 'non-commercial-and-evaluation',
    dropdownMenu: true,
    filters: true,
    manualColumnResize: true,
    contextMenu: true,
    multiColumnSorting: true,
    afterChange: (change, source) => {
      if (source === 'loadData') {
        return; //don't save this change
      }
      if (!change) {
        return;
      }

      let rowChange = change[0][0],
        colChangenumber = change[0][1],
        newValue = change[0][3];

      console.log({ change });

      this.dataAfterChanges.push(newValue);
      console.log(this.dataAfterChanges);
    },
  };

  getStaffList() {
    this.apiService.getInfoList().subscribe({
      next: (res) => {
        this.notification = 'Lấy dữ liệu thành công';
        this.staffList = res.data;
      },

      error: (error) => {
        console.log(error);
      },
    });
  }

  getConfigList() {
    this.apiService.getConfigList().subscribe({
      next: (res) => {
        this.columnWidths = res.widths;
      },

      error: (error) => {
        console.log(error);
      },
    });
  }

  updateData() {
    console.log('Update');
  }

  ngOnInit(): void {
    this.getStaffList();
    this.getConfigList();
  }
}
