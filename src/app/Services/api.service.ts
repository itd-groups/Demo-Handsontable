import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  REST_API_SERVER = 'https://m30zv.mocklab.io';
  constructor(private httpClient: HttpClient) {}

  getInfoList() {
    const url = `${this.REST_API_SERVER}/staffs`;
    return this.httpClient.get<any>(url);
  }

  getConfigList() {
    const url = `${this.REST_API_SERVER}/config-width`;
    return this.httpClient.get<any>(url);
  }
}
